﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DragDropTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MyViewModel();
        }

        private void SecondList_Drop(object sender, DragEventArgs e)
        {
            MyDataObject data = (MyDataObject) e.Data.GetData(typeof(MyDataObject));
            ListBox parent = (ListBox) sender;

            MyViewModel model = (MyViewModel) parent.DataContext;
            model.SecondList.Add(data);
        }


        private void FirstList_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                ListBox parent = (ListBox) sender;
                MyDataObject obj = (MyDataObject) FirstList.SelectedItem;
                DragDrop.DoDragDrop(parent, obj, DragDropEffects.Copy);
            }
        }
    }
}