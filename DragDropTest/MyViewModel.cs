﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DragDropTest
{
    class MyViewModel
    {
        public ObservableCollection<MyDataObject> FirstList { get; set; }
        public ObservableCollection<MyDataObject> SecondList { get; set; }

        public MyViewModel()
        {
            FirstList = new ObservableCollection<MyDataObject>();
            SecondList = new ObservableCollection<MyDataObject>();

            FirstList.Add(new MyDataObject {Name = "Tabea", Type = "Kind"});
            FirstList.Add(new MyDataObject {Name = "Pascal", Type = "Kind"});
            FirstList.Add(new MyDataObject {Name = "Sarah", Type = "Kind"});
            FirstList.Add(new MyDataObject {Name = "Sandra", Type = "Mutter"});
        }
    }

    public class MyDataObject
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}